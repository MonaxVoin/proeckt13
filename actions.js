
const BASE_URL = 'http://api.persik.by/v2/';
const genresSelect = document.querySelector('#genres');
const tvshowsContainer = document.querySelector(`.tvshows`);
let allChannels = [];



// const  genres = await this.getGenres();

genresSelect.addEventListener('change', changeGenre);













// getGenres().then(genres => renderOptions(genres));
// getChannels().then(channels => {
//     allChannels = channels;
//     renderChannels(channels);

// });
console.log('Start loading...')
Promise.all([getGenres(), getChannels()])
    .then(([genres, channels]) => {
        allChannels = channels;
        console.log(allChannels);
        renderOptions(genres);
        renderChannels(channels);

    }).finally(() => console.log('Loading finished'));



function getGenres() {//массив жанров
    const xhr = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        xhr.open('Get', BASE_URL.concat('categories/channel'))
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    const allGenre = {
                        id: 1,
                        name: 'Все жанры'
                    };
                    const data = JSON.parse(xhr.responseText);
                    data.unshift(allGenre);
                    resolve(data);
                } else {
                    reject(xhr.status);
                }
            }
        }



        xhr.send();
    });

}


function getChannels() {
    const xhr = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        xhr.open('GET', BASE_URL.concat('content/channels'));
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    const data = JSON.parse(xhr.responseText);
                    console.log(data);
                    resolve(data.channels);
                } else {
                    reject(xhr.status);
                }
            }
        }
        xhr.send();
    });
}

function getTvshows(channelId) {
    const xhr = new XMLHttpRequest();
    return new Promise((resolve, reject) => {
        const startDate = moment().add(-3,'days').format('YYYY-MM-DD');//2020-10-09
        const endDate = moment().format('YYYY-MM-DD');
        xhr.open('GET', BASE_URL.concat(`epg/tvshows?channels[]=${channelId}&from=${startDate}&to=${endDate}`));
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    const data = JSON.parse(xhr.responseText);
                    resolve(data.tvshows.items);

                }else {
                    reject(status);
                }
            }
        };
        xhr.send();
    });
}


function getOptionTag(genre) {
    const option = document.createElement('option');
    option.value = genre.id;
    option.innerText = genre.name;
    return option;
}



function renderOptions(genres) {

    // const allGenreOptonTag = getOptionTag(allGenre);
    // genresSelect.appendChild(allGenreOptonTag);


    genres.forEach(item => {
        const optionTag = getOptionTag(item);
        genresSelect.appendChild(optionTag);
    });

}


function changeGenre(event) {
    const selectedGenredId = Number(event.target.value);
    let filteredChannels = allChannels;
    if (selectedGenredId !== 1) {
        filteredChannels = allChannels.filter(item => item.genres.includes(selectedGenredId))
    }

    renderChannels(filteredChannels);

}






function getChannelTag(channel) {
    const channelTag = document.createElement('div');
    channelTag.classList.add('channel');

    const logoTag = document.createElement('img');
    logoTag.src = channel.logo;


    const numberTag = document.createElement('span');
    numberTag.innerText = channel.channel_id;


    const nameTag = document.createElement('span');
    nameTag.innerText = channel.name;


    channelTag.append(logoTag, numberTag, nameTag);


    channelTag.addEventListener('click', () => onChannelSelect(channel.channel_id));
    return channelTag;
}


function onChannelSelect(channelId) {
    getTvshows(channelId).then(res =>drawTvshows(res));
}


function renderChannels(channels) {
    const channelsContainer = document.querySelector('#channels');
    channelsContainer.innerText = '';

    channels.forEach(item => {
        const channelCard = getChannelTag(item);
        channelsContainer.appendChild(channelCard);
    });
}


function getTvShowTag(tvshow) {
    const tvshowDiv = document.createElement('div');
    tvshowDiv.classList.add('tvshow');



    const titleSpan = document.createElement('span');
    titleSpan.classList.add('tvshow-title');
    titleSpan.innerHTML = tvshow.title;


    const startSpan = document.createElement('span');
    startSpan.innerText = moment.unix(tvshow.start).format('HH:mm');


    const endSpan = document.createElement('span');
    endSpan.innerText = moment.unix(tvshow.stop).format('HH:mm');


    const timeLine = document.createElement('div');
    timeLine.classList.add('timeline');
    timeLine.append(startSpan, endSpan);


    tvshowDiv.append(titleSpan,timeLine);
    return tvshowDiv;

}


function drawTvshows(tvshows){
    tvshowsContainer.innerHTML = '';
    tvshows.forEach(item => {
        const tvshowTag = getTvShowTag(item);
        tvshowsContainer.appendChild(tvshowTag);
    });
}
